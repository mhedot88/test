export interface Commandes {
    _id:string
    name:  string
    nameprod:  string
    imageUrl: string
    quantity:  number
    date:  Date
}
