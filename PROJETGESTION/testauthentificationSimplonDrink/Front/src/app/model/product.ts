export interface Product {
    _id?:string
    titre?:string
    contenu?:string
    name?:string
    imageUrl?:string
    category?:string
    quantity?:number
    description?:string
    date?:Date
    etat?:string
    NomConsommateur?:string
    
    
}
