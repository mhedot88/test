export interface Utilisateur {
    _id?:string,
    nom:string,
    prenom:string,
    pseudo:string,
    preference:string,
    promotion:string,
    email:string,
    lieu:string
}
