export interface Post {
    _id?:string,
    title:string,
    body:string,
    author:string,
    date:Date,
    imgUrl:string
}
