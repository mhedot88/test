export interface Commandes {
    id?: number;
    name: string;
    nameprod: string;
    imageUrl: string;
    quantity: number;
    date: Date;
}
