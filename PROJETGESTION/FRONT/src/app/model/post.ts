export interface Post {
    _id?:string,
    titre:string,
    contenu:string,
    author:string,
    date:Date,
    imgUrl:string
}
