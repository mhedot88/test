export interface Demande {
    Nom:string,
    description:string,
    pseudo:string,
    date:Date,
    type:string,
    nature:Boolean
}