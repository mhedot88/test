export interface Utilisateur {
    id?:number,
    nom:string,
    prenom:string,
    pseudo:string,
    preference:string,
    promotion:string,
    email:string,
    lieu:string,
    img:string
}

