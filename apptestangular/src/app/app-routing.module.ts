import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesComponent } from './component/articles/articles.component';
import { DatatestDetailComponent } from './component/datatest-detail/datatest-detail.component';
import { DatatestComponent } from './component/datatest/datatest.component';
import { DiagramsComponent } from './component/diagrams/diagrams.component';
import { DocfileComponent } from './component/docfile/docfile.component';
import { GeoloctutoComponent } from './component/geoloctuto/geoloctuto.component';
import { LefletmapComponent } from './component/lefletmap/lefletmap.component';
import { MakephotoComponent } from './component/makephoto/makephoto.component';
import { MessageetatComponent } from './component/messageetat/messageetat.component';
import { PaginationexempleComponent } from './component/paginationexemple/paginationexemple.component';
import { RecaptchaformComponent } from './component/recaptchaform/recaptchaform.component';
import { TranslationcomponentComponent } from './component/translationcomponent/translationcomponent.component';

const routes: Routes = [
  {path:'routefile',component:DocfileComponent},
  {path:'makephoto',component:MakephotoComponent},
  // {path:'recaptcha',component:RecaptchaformComponent},
  {path:'translation',component:TranslationcomponentComponent},
  {path:'map',component:LefletmapComponent},
  {path:'geoloc',component:GeoloctutoComponent},
  {path:'datatest',component:DatatestComponent},
  {path:'datatest-detail/:id',component:DatatestDetailComponent},
  {path:'diagram',component:DiagramsComponent},
  {path:'pagination',component:PaginationexempleComponent},
  {path:'messageetat',component:MessageetatComponent},
  {path:'article',component:ArticlesComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
