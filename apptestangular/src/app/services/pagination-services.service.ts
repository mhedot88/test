import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Data } from  '../model/data';
@Injectable({
  providedIn: 'root'
})
export class PaginationServicesService {
private api='http://localhost:3241/photos';
  constructor(public http: HttpClient) { }
  public get(): Observable<Data[]>{
    return this.http.get<Data[]>(this.api);
  }
  public getid(id:number):Observable<Data[]>{ 
     console.log(this.api+'/'+id);
    return this.http.get<Data[]>(this.api+'/'+id);
  
    
  }
}
