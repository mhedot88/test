import { TestBed } from '@angular/core/testing';

import { PaginationServicesService } from './pagination-services.service';

describe('PaginationServicesService', () => {
  let service: PaginationServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaginationServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
