import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecaptchaformComponent } from './recaptchaform.component';

describe('RecaptchaformComponent', () => {
  let component: RecaptchaformComponent;
  let fixture: ComponentFixture<RecaptchaformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecaptchaformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecaptchaformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
