import { Component, OnInit } from '@angular/core';
import {NgForm } from '@angular/forms';
// import { debug } from 'console';

@Component({
  selector: 'app-recaptchaform',
  templateUrl: './recaptchaform.component.html',
  styleUrls: ['./recaptchaform.component.css']
})
export class RecaptchaformComponent implements OnInit {
  token: string|undefined;



  constructor() {
    this.token=undefined;
   }
   public send(form:NgForm):void{
     if(form.invalid){
       for (const control of Object.keys(form.controls)) {
          form.controls[control].markAsTouched()
       }
       return
     }
     console.debug(`Token [${this.token}] generated`)
   }

  ngOnInit(): void {
  }

}
