import { Component, OnInit } from '@angular/core';
import { Pet } from "../../model/pet";
@Component({
  selector: 'app-petform',
  templateUrl: './petform.component.html',
  styleUrls: ['./petform.component.css']
})
export class PetformComponent implements OnInit {
  pet:Pet[]=[];
  species=['fish','dog','cat','lama'];
  model= new Pet(1,'Boubou','dog','red');
  submitted:boolean=false;
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(){
    this.submitted=true;
  }
  edit(){
    this.submitted=false;
  }
}
