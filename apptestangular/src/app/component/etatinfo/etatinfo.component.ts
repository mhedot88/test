import { Component, OnInit } from '@angular/core';
import { EchangemessageService } from 'src/app/service/echangemessage.service';

@Component({
  selector: 'app-etatinfo',
  templateUrl: './etatinfo.component.html',
  styleUrls: ['./etatinfo.component.css']
})
export class EtatinfoComponent implements OnInit {
message:string='';
  constructor(public serv:EchangemessageService) { }

  ngOnInit(): void {
    this.serv.receiveMessage().subscribe((terme)=>{
      this.message=terme;
      console.log(terme);
      
    })
  }


}
