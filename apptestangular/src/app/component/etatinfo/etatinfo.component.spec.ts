import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtatinfoComponent } from './etatinfo.component';

describe('EtatinfoComponent', () => {
  let component: EtatinfoComponent;
  let fixture: ComponentFixture<EtatinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtatinfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtatinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
