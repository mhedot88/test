import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formtest',
  templateUrl: './formtest.component.html',
  styleUrls: ['./formtest.component.css']
})
export class FormtestComponent implements OnInit {
   dataList:any=['Roi','Prince','Duc','Marquis','Comte','Baron','Seigneur','Chevalier'];
   listforce:any=['Invincible','Fort','Moyen','Faible','Negligeable'];
  //  public siteKey:any;



  constructor(public formBuilder:FormBuilder) { }
  form=new FormGroup({
    noblesse:new FormControl('',Validators.required),
    puissance:new FormControl('',Validators.required),
    date:new FormControl('',Validators.required)
  })

  get f(){
    return this.form.controls;
  }
  submit(){
    console.log(this.form.value); 
  }
  changetitreNoblesse(e:any){
    console.log(e.target.value);
  }

  ngOnInit(): void {
    // this.form=this.formBuilder.group({
    //   recaptcha:['',Validators.required]
    // });
  }

}
