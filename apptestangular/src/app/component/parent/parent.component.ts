import { Component, OnInit,ViewChild,AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
// import { ReversePipe } from 'ngx-pipes';
import { EltservicesService } from 'src/app/service/eltservices.service';
import { MapsService } from 'src/app/service/maps.service';
import {ChildComponent} from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css'],
 
})
export class ParentComponent implements OnInit, AfterViewInit {
  //
  titre:string='Titre test';
   //cas attribute binding sert a ajouter  propriété sur tout un element si onsale=true
  onsale:boolean=false;
  //cas two way binding
  userName:string='';
  // cas viewchild
  @ViewChild(ChildComponent) child:any;
  // template variable
  phone:string='';
  // directive attribute type NgClass et NgStyle
  // sert a ajouter ou supprimer une classe si condition et juste

  isSpeciale:boolean=false; 
   // cas du service 
   valeurden:number=5;
valeurmontrer:number=0;

// partie ngx pipes :
textessai:string='text';


  constructor(private logservice:EltservicesService,private router:Router,public mapservice: MapsService) { }

  ngOnInit(): void {
    this.logservice.go();
  this.valeurmontrer=  this.logservice.x*this.valeurden;
  // this.mapservice.getLocation().subscribe(data =>{
  //   console.log(data);
    
  // })
  }
  //cas @Viewchild
  ngAfterViewInit():void{
    console.log("1",this.child.message);
    this.child.createMessage();
    console.log("2",this.child.message);
    
  }  
  //cas output child to parent
  receive(msg:number|string){
      console.log(msg);
      
  }
  //template variable
  callphone(phone:string){
    console.log(phone);
    
    if (phone==='true') {
      alert('hello'+' '+phone);
      
    } else{
      alert('hello');
    }
  }
  //ng classe
  state(){
    console.log(this.isSpeciale );
    
    if (this.isSpeciale==true) {
      this.isSpeciale=false;
    }
    else {
      this.isSpeciale=true;
    }
    console.log(this.isSpeciale );
  }
  // aller vers une page en click
  go(){
    this.router.navigate(['/routefile']);
  }
  go2(){
    this.router.navigate(['/makephoto']);
  }

// recup data de input
select(event:MouseEvent){
  const index:number= +(event.target as HTMLInputElement).value;
  console.log(index,"event target:",event.target, "event:",event.clientX );
  

}



}
