import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MakephotoComponent } from './makephoto.component';

describe('MakephotoComponent', () => {
  let component: MakephotoComponent;
  let fixture: ComponentFixture<MakephotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MakephotoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MakephotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
