import { Component,OnInit } from '@angular/core';
import {WebcamImage} from 'ngx-webcam';
import {Subject, Observable} from 'rxjs';

@Component({
  selector: 'app-makephoto',
  templateUrl: './makephoto.component.html',
  styleUrls: ['./makephoto.component.css']
})
export class MakephotoComponent implements OnInit {
  webcamImage: WebcamImage |any;
  seconds:number|any;
  trigger: Subject<void> = new Subject<void>();

isok:boolean=false;

  constructor() { }

  ngOnInit(): void {
  }
  photo(statut:boolean){ 
    console.log(this.isok);
   console.log(statut);
   
    
    if (statut===false) {
      this.isok=true;
    }
    if (statut===true) {
      
      this.isok=false;
      
 }
 
  }
  takepicture():void{
    this.seconds=3;
    setTimeout(()=>{
      this.seconds=2;
      setTimeout(()=>{
        this.seconds=1;
        setTimeout(()=>{
          this.trigger.next();
          this.seconds=null;
        },2000)
      },2000)
    },2000)
  }
  handleImage(webcamImage:WebcamImage):void{
    this.webcamImage=webcamImage;
    console.log( this.webcamImage.imageAsDataUrl);
    
  }
  get triggerObsrvable():Observable<void>{
    return this.trigger.asObservable();
  }
  edit(){
    this.webcamImage='';
  }
  // triggerSnapshot(): void {
  //   this.trigger.next();
  //  }
  //  handleImage(webcamImage: WebcamImage): void {
  //   console.info('Saved webcam image', webcamImage);
  //   this.webcamImage = webcamImage;
  //  }
    
  //  public get triggerObservable(): Observable<void> {
  //   return this.trigger.asObservable();
  //  }
}
