import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationexempleComponent } from './paginationexemple.component';

describe('PaginationexempleComponent', () => {
  let component: PaginationexempleComponent;
  let fixture: ComponentFixture<PaginationexempleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginationexempleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationexempleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
