import { Component, OnInit } from '@angular/core';
import { Data } from 'src/app/model/data';
import { PaginationServicesService } from 'src/app/services/pagination-services.service';

@Component({
  selector: 'app-paginationexemple',
  templateUrl: './paginationexemple.component.html',
  styleUrls: ['./paginationexemple.component.css'],
})
export class PaginationexempleComponent implements OnInit {
  visisbilite:boolean=true;
  // pagination manuel

  datapages: Data[] = [] || undefined;
  valmin: number = 1;
  valmax: number = 5;
  valnumberelt: number = 5;
  position: number = 1;
  limitmin: number = 0;
  limitemax: number = 1;
  // search
  value: any;
  idsearch: number = 1;
  elts:number=1;
  constructor(private dataserv: PaginationServicesService) {}
  ngOnInit(): void {
    this.dataserv.get().subscribe((datapages) => {
      this.datapages = datapages;
      // console.log(this.datapages.length);
      this.limitemax = Math.trunc(
        this.datapages.length / this.valnumberelt + 1
      );
      console.log(this.limitemax);
    });
  }
  avance() {
    this.valmax = this.valmax + this.valnumberelt;
    this.valmin = this.valmin + this.valnumberelt;
    this.position = this.position + 1;
  }
  retour() {
    this.valmax = this.valmax - this.valnumberelt;
    this.valmin = this.valmin - this.valnumberelt;
    this.position = this.position - 1;
  }
  // searching by id:
  onSaerch($event: any) {
    if (this.value!='') {
      this.value = $event.target.value;
    for (let index = 0; index < this.datapages.length; index++) {
      
      if (this.value === this.datapages[index].title) {
        this.idsearch = index+1;
        console.log(this.idsearch ) ;
        
      }
    }
    console.log(this.idsearch);
    this.getid(this.idsearch);
    }
    if (this.value=='') {
      this.visisbilite=true;
      location.reload();
      
    }
    
  }
  getid(id:number) {
    console.log(id);
    this.elts=id-1;
   this.visisbilite=false;
   
  //  console.log(this.datapages[2]);
   
  }
}
