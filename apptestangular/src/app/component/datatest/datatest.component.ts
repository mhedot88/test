import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Elttest } from "../../model/elttest";
@Component({
  selector: 'app-datatest',
  templateUrl: './datatest.component.html',
  styleUrls: ['./datatest.component.css'],
 
})

export class DatatestComponent implements OnInit {
  datas:Elttest[]=[
    {
      'id':1,
      'name':'cas 1',
      'detail':' cest le test 1'
    },
    {
      'id':2,
      'name':'cas 2',
      'detail':' cest le test 2'
    }


  ];
  files: File[] = [];
  constructor(private route:Router) { }

// partie dropzone


	onSelect(event:any) {
		console.log(event,this.files);
    this.files.push(...event.addedFiles);
    console.log(event,this.files);
	}

	onRemove(event:any) {
		console.log(event,this.files);
    this.files.splice(this.files.indexOf(event), 1);
    console.log(this.files);
    
	}
// partie navigation
  voir(num:number){
    this.route.navigate(['datatest-detail',num])
  }
  ngOnInit(): void {
  }

}
