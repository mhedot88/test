import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Elttest } from 'src/app/model/elttest';

@Component({
  selector: 'app-datatest-detail',
  templateUrl: './datatest-detail.component.html',
  styleUrls: ['./datatest-detail.component.css']
})
export class DatatestDetailComponent implements OnInit {
  datas:Elttest[]=[ {
    'id':1,
    'name':'cas 1',
    'detail':' cest le test 1'
  },
  {
    'id':2,
    'name':'cas 2',
    'detail':' cest le test 2'
  }
]
idsearched:number=0;
lengthdata:number=0;
  constructor(private routeParams:ActivatedRoute) { }

  ngOnInit(): void {
  this.lengthdata=  this.datas.length;
    this.routeParams.params.subscribe(elt=>{
      this.idsearched= +elt['id'];
      console.log(elt['id']);
      
    })
  }

}
