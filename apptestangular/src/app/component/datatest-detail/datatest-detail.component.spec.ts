import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatatestDetailComponent } from './datatest-detail.component';

describe('DatatestDetailComponent', () => {
  let component: DatatestDetailComponent;
  let fixture: ComponentFixture<DatatestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatatestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatatestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
