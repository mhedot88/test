import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-photoupload',
  templateUrl: './photoupload.component.html',
  styleUrls: ['./photoupload.component.css']
})
export class PhotouploadComponent implements OnInit {
  imageSrc:string='';
  myForm= new FormGroup({
    name:new FormControl('',[Validators.required]),
    file:new FormControl('', Validators.required),
    fileSource: new FormControl('',[Validators.required])
  });
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }
  get f(){
    return this.myForm.controls;
  }
  onFileChange(event:any){
    const reader= new FileReader();
    if(event.target.files && event.target.files.length)
    {
      const [file]=event.target.files;
      reader.readAsDataURL(file);
      reader.onload= ()=>{
        this.imageSrc=reader.result as string;
        this.myForm.patchValue({
          fileSource: reader.result
        })
      }
    }
  }
  submit(){
    console.log(this.myForm, this.myForm.value);console.log("imgcode",this.imageSrc);
    this.http.post('',this.myForm.value).subscribe((res)=>{console.log(res)
    
    })
    
  }
}
