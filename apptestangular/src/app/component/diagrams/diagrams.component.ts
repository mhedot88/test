import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'app-diagrams',
  templateUrl: './diagrams.component.html',
  styleUrls: ['./diagrams.component.css'],
})
export class DiagramsComponent implements OnInit {
  // partie sweetAlert2
  Text: any[] = [{ text: 'Nombre de personne en Millions' }];
  highcharts = Highcharts;

  // chartOptions: Highcharts.Options = {
  //   title: {
  //     text: "Demographie"
  //   },
  //   xAxis: {
  //     title: {
  //       text: 'ANNEE'
  //     },
  //     categories: ["1815","1910"]
  //   },
  //   yAxis: {
  //     title: {
  //       text: "Millions Habitants"
  //     }
  //   },
  //   series: [{
  //     name:'France',
  //     data: [30.573, 41.350],
  //     type: 'spline'
  //   },{
  //     name: 'Russie',
  //     data: [48.500,143.900],
  //     type: 'spline'
  //   }]
  // }
  // chartOptions: Highcharts.Options={
  //   chart:{
  //     type:"spline"
  //   },
  //   title: {
  //     text: "Demographie"
  //   },
  //   xAxis:{

  //     categories: ["1815","1910"]
  //   },
  //   yAxis: {
  //     title: {
  //       text: "Millions Habitants"
  //     }
  //   },plotOptions:{
  //     series:{dataLabels:{enabled:true}}
  //   },tooltip:{
  //     valueSuffix:"Hab"
  //   }
  //   ,series: [{
  //     name:'France',
  //     data: [30.573, 41.350],
  //     type: 'spline'
  //   },{
  //     name: 'Russie',
  //     data: [48.500,143.900],
  //     type: 'spline'
  //   }]
  // }
  chartOptions: Highcharts.Options = {   
    chart: {
       type: 'area'
    },
    title: {
       text: 'Average fruit consumption during one week'
    },
    subtitle : {
       style: {
          position: 'absolute',
          right: '0px',
          bottom: '10px'
       }
    },

    xAxis:{
       categories: ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'] 
    },
    yAxis : {
       title: {
          text: 'Number of units'
       }
    },
   
    plotOptions : {
       area: {
          fillOpacity: 0.5 
       }
    },
    credits:{
       enabled: false
    },
    series: [
      {
            // name: 'Russie',
            data: [3,4,3,5,7,4,10],
            type: 'area'
          },
       {
        
        data: [1,2,3,4,5,6,4],
        type: 'area'
       }
    ]
 };


  constructor() {}

  ngOnInit(): void {}
}
