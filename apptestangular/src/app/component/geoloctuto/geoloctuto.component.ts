import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
@Component({
  selector: 'app-geoloctuto',
  templateUrl: './geoloctuto.component.html',
  styleUrls: ['./geoloctuto.component.css']
})
export class GeoloctutoComponent implements OnInit {

  constructor() { }
  x:number|undefined
  marker:any;
  othermarker:any;
  minLayer:any=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    minZoom:0,
    maxZoom:25,
    attribution:'&copy,<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  })

  cartedataliste=[{
    "lieu":"Carthage",
    "lat":36.8610800,
    "lon":10.3316100
  },
  {
    "lieu":"Sagunte",
    "lat":39.6801,
    "lon":0.2784
  },
  {
    "lieu":"Trasimène",
    "lat":43.1296,
    "lon":12.0947
  },{
    "lieu":"Cannes",
    "lat":41.2871,
    "lon":16.1500
  },
  {
    "lieu":"Anagni",
    "lat":41.7453,
    "lon":13.1514

  },{
    "lieu":"",
    "lat":0,
    "lon":0

  }
]



  ngOnInit(): void {
    if(!navigator.geolocation){
      console.log('location n\'est pas supporté' );
      
    }
    if (navigator.geolocation) {
      console.log('location est supporté');
      navigator.geolocation.getCurrentPosition((position)=>{
        console.log(position, typeof(position.coords.latitude));
        this.x=position.coords.latitude;
        console.log(this.x);
        let mymap=L.map('map').setView([position.coords.latitude,position.coords.longitude],13);
        this.minLayer.addTo(mymap);
        this.marker=L.marker([position.coords.latitude,position.coords.longitude]).addTo(mymap).bindPopup('city where I leave').openPopup();
      for (let index = 0; index < this.cartedataliste.length; index++) {
        this.othermarker=L.marker([this.cartedataliste[index].lat,this.cartedataliste[index].lon]).addTo(mymap).bindPopup(this.cartedataliste[index].lieu)
        
      }
      
  //       othermarker(){
  //   let marker=L.marker([lat,lon]);
  //   marker.addTo(this.map).bindPopup(text);
  // }
      })
      console.log(this.x);
      this.WatchPosition();
    }
    console.log(this.x);
    
  }
  
  WatchPosition(){
  let  desLat=0;
  let desLon=0;
  let id= navigator.geolocation.watchPosition((position)=>{
      console.log(position, typeof(position.coords.latitude));
      this.x=position.coords.latitude;
      console.log(this.x);
      if (position.coords.latitude===desLat) {
        navigator.geolocation.clearWatch(id);
      }
    },(err)=>{console.log(err);
    },{
      enableHighAccuracy:false,
      timeout:3000,
      maximumAge:0
    })
  }

}
