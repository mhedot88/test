import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeoloctutoComponent } from './geoloctuto.component';

describe('GeoloctutoComponent', () => {
  let component: GeoloctutoComponent;
  let fixture: ComponentFixture<GeoloctutoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeoloctutoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeoloctutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
