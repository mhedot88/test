import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
number:number=1;
tab:number[]=[];
@Input() texte:string='';
//partie child to parent
@Output() messageEvent =new EventEmitter<number>();
//partie @ViewChild
 message='message from child';

  constructor() { }

  ngOnInit(): void {
  }
  //parent to child
  sendMessage(value:number){
  this.createValue();
  value=this.number;
  console.log(value);
  
    this.messageEvent.emit(value);
  }
  createValue(){
  this.number= (Math.random())*10;
  console.log(this.number);
  
  }
  //partie @Viewchild
  createMessage(){
    this.message="second message from child";
  }

}
