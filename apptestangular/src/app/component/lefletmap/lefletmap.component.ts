import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
@Component({
  selector: 'app-lefletmap',
  templateUrl: './lefletmap.component.html',
  styleUrls: ['./lefletmap.component.css']
})
export class LefletmapComponent implements OnInit {

  cartedataliste=[{
    "lieu":"Carthage",
    "lat":36.8610800,
    "lon":10.3316100
  },
  {
    "lieu":"Sagunte",
    "lat":39.6801,
    "lon":0.2784
  },
  {
    "lieu":"Trasimène",
    "lat":43.1296,
    "lon":12.0947
  },{
    "lieu":"Cannes",
    "lat":41.2871,
    "lon":16.1500
  },
  {
    "lieu":"Anagni",
    "lat":41.7453,
    "lon":13.1514

  },{
    "lieu":"",
    "lat":0,
    "lon":0

  }
]
  positiondata:any={lat:null, lon:null, lieu:''};

  cartedata:any={
    lat:41.9028,
    lon:12.4964,
    lieu:"Ville de Rome -Siege en 1054"
  }
  zoomLevel:number=7;
  minLayer:any=L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    minZoom:0,
    maxZoom:25,
    attribution:'&copy,<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  })
  map:any;
  array:any=[];
  icons:any=new L.Icon({
    iconUrl:'https://www.pngitem.com/pimgs/m/284-2844723_symbol-that-represents-war-hd-png-download.png',
    iconSize:[10,12],

  })
  text:string='Ville de Rome -Siege en 1054'
  constructor() { }
// partie Map 1 data sur carte et 2 geolocalisation
// partie 2
  ngOnInit(): void {
   this.createMap();
  }
  // partie 1

  createMap(){
    //partie ajout création et centrage map
    if (!navigator.geolocation) {
      console.log('location n\'est pas supporté');}
    let latitude;
    let longitude;
    this.array;
    navigator.geolocation.watchPosition((position )=>{  

    latitude=position.coords.latitude;
    longitude=position.coords.longitude;
    this.array.push(latitude);
    this.array.push(longitude);
   
    })
    console.log(this.array,);
    let latitudegeo=this.array[0];
    let longitudegeo=this.array[1];
    console.log(latitudegeo, longitudegeo);

    
     console.log(this.map),
    this.map=L.map('map',{
     
      
      center:[this.array[0],this.array[1]],
      zoom:this.zoomLevel
    });
    this.minLayer.addTo(this.map);

 
    // partie ajout marker
    for (let index = 0; index < this.cartedataliste.length; index++) {
      console.log(this.cartedataliste[index]);
      this.addMarker(this.cartedataliste[index].lat,this.cartedataliste[index].lon,
        this.cartedataliste[index].lieu);
      
    }
    
    // this.addMarker(this.cartedata.lat,this.cartedata.lon,
    //   this.cartedata.lieu);
  }
  addMarker(lat:any,lon:any,text:string){
    let marker=L.marker([lat,lon],{icon:this.icons});
    marker.addTo(this.map).bindPopup(text);
  }
}
