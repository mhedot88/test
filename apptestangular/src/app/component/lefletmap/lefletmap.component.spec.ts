import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LefletmapComponent } from './lefletmap.component';

describe('LefletmapComponent', () => {
  let component: LefletmapComponent;
  let fixture: ComponentFixture<LefletmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LefletmapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LefletmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
