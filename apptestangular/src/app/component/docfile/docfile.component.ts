import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docfile',
  templateUrl: './docfile.component.html',
  styleUrls: ['./docfile.component.css']
})
export class DocfileComponent implements OnInit {
  visible:boolean=true
  participants:any=[
    {
      nom:'Hedot',
      prenom:'Mathieu',
      age:'34'
    },
    {
      nom:'Montlaur',
      prenom:'adrien',
      age:'32'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }
  visibilite(){
this.visible=false;
  }
}
