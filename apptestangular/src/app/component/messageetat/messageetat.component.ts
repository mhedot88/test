import { Component, OnInit } from '@angular/core';
import { EchangemessageService } from 'src/app/service/echangemessage.service';

@Component({
  selector: 'app-messageetat',
  templateUrl: './messageetat.component.html',
  styleUrls: ['./messageetat.component.css']
})
export class MessageetatComponent implements OnInit {

  constructor(public serv:EchangemessageService) { }

  ngOnInit(): void {
  }
  sendPaysName(message:string){
    this.serv.sendCountryName(message);
  }

}
