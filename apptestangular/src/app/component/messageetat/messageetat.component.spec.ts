import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageetatComponent } from './messageetat.component';

describe('MessageetatComponent', () => {
  let component: MessageetatComponent;
  let fixture: ComponentFixture<MessageetatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessageetatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageetatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
