import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslationcomponentComponent } from './translationcomponent.component';

describe('TranslationcomponentComponent', () => {
  let component: TranslationcomponentComponent;
  let fixture: ComponentFixture<TranslationcomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranslationcomponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslationcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
