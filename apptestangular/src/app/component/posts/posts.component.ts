import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  Posts:any=[];
  constructor(public http:HttpClient) { }

  ngOnInit(): void {
    const languetexte=localStorage.getItem('Langue');
    console.log(languetexte);

    const headers= new HttpHeaders({
      'Accept-Language': languetexte || 'En'
    })
    
    this.http.get(' http://localhost:3241/infos',{headers:headers}).subscribe( 
      (res)=>{this.Posts=res
      }
    )
  }

}
