import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './component/parent/parent.component';
import { ChildComponent } from './component/child/child.component';
import { PetformComponent } from './component/petform/petform.component';
import { NgxPrintModule } from "ngx-print";
import { DocfileComponent } from './component/docfile/docfile.component';
import { MakephotoComponent } from './component/makephoto/makephoto.component';
import { WebcamModule } from 'ngx-webcam';
import { PhotouploadComponent } from './component/photoupload/photoupload.component';
import { HttpClientModule } from '@angular/common/http';
import { FormtestComponent } from './component/formtest/formtest.component';
import { DatatestComponent } from './component/datatest/datatest.component';
import { DatatestDetailComponent } from './component/datatest-detail/datatest-detail.component';
import { NgxCaptchaModule } from "ngx-captcha";
// import { RECAPTCHA_SETTINGS, RecaptchaFormsModule, RecaptchaModule, RecaptchaSettings } from "ng-recaptcha";
import { environment } from "../environments/environment";
import { RecaptchaformComponent } from './component/recaptchaform/recaptchaform.component';
import { NgxDropzoneModule } from "ngx-dropzone";
import { LefletmapComponent } from './component/lefletmap/lefletmap.component';
import { TranslationcomponentComponent } from './component/translationcomponent/translationcomponent.component';
import { PostsComponent } from './component/posts/posts.component';
import { GeoloctutoComponent } from './component/geoloctuto/geoloctuto.component';
import { DiagramsComponent } from './component/diagrams/diagrams.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { PaginationexempleComponent } from './component/paginationexemple/paginationexemple.component';
import { MessageetatComponent } from './component/messageetat/messageetat.component';
import { EtatinfoComponent } from './component/etatinfo/etatinfo.component';
import { ArticlesComponent } from './component/articles/articles.component';
import { HostdirectiveDirective } from './directives/hostdirective.directive';

// import { ChartsModule } from 'ng2-charts';
// import { NgPipesModule, ReversePipe } from 'ngx-pipes';
// import { AgmCoreModule } from "@agm/core";

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    PetformComponent,
    DocfileComponent,
    MakephotoComponent,
    PhotouploadComponent,
    FormtestComponent,
    DatatestComponent,
    DatatestDetailComponent,
    RecaptchaformComponent,
    LefletmapComponent,
    TranslationcomponentComponent,
    PostsComponent,
    GeoloctutoComponent,
    DiagramsComponent,
    PaginationexempleComponent,
    MessageetatComponent,
    EtatinfoComponent,
    ArticlesComponent,
    HostdirectiveDirective
  ],
  imports: [
    BrowserModule,
     AppRoutingModule,NgxCaptchaModule,FormsModule,NgxPrintModule,WebcamModule,ReactiveFormsModule,HttpClientModule, NgxDropzoneModule,HighchartsChartModule
    //  AgmCoreModule.forRoot({
    //   apiKey:'AIzaSyCxtUziNqI8T4HgBGZXdl6FxxrgTT0TCOg'
    //  })
    // RecaptchaModule,RecaptchaFormsModule
  ],
  providers: [
    
  //   {
  //   provide: RECAPTCHA_SETTINGS,
  //   useValue:{
  //     siteKey:environment.recaptcha.sitekey
  //   } as RecaptchaSettings
  // }
],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
