import { TestBed } from '@angular/core/testing';

import { EchangemessageService } from './echangemessage.service';

describe('EchangemessageService', () => {
  let service: EchangemessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EchangemessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
