import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'highcharts';
import { forkJoin, Observable } from 'rxjs';

import { Article } from '../model/articles';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(protected http:HttpClient) { }
  public getArticles():Observable<Article[]>{
    return this.http.get<Article[]>('http://localhost:3241/articles')
    
  }
  getAuthorOfTheMonth(): Observable<Object> {
    return this.http.get<Object>('http://localhost:3241/authorofthemonth');
  }

}
