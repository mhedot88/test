import { TestBed } from '@angular/core/testing';

import { EltservicesService } from './eltservices.service';

describe('EltservicesService', () => {
  let service: EltservicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EltservicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
