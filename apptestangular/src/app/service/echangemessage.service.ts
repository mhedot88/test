import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EchangemessageService {
  public subject=new Subject<string>();
  constructor() { }
  sendCountryName(message:any){
    this.subject.next(message);
  }
  receiveMessage():Observable<string>{
  return this.subject.asObservable()
  }
}
