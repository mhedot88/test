export interface Pet {
    id:number,
    name:string,
    specie:string,
    color:string
}
//necesaire si formulaire de type template driven form
export class Pet {
    constructor(
        public id:number,
        public name:string,
        public specie:string,
        public color:string

    ) {
       
        
    }
}