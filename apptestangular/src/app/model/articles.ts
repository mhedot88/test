export interface Article {
    author:string,
    title:string,
    body:string
}
