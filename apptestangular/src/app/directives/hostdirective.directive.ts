import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHostdirective]'
})
export class HostdirectiveDirective {

  constructor( public element:ElementRef, public render:Renderer2) { }
 @HostListener('mouseenter') onmouseenter(){
    this.render.setStyle(this.element.nativeElement,'border','1px solid red');
    this.render.setStyle(this.element.nativeElement,'background-color','rgba(255, 0, 0, 0.2)');
  }

  @HostListener('mouseover') onmouseover(){
    this.render.setStyle(this.element.nativeElement,'border','1px solid transparent');
    this.render.setStyle(this.element.nativeElement,'background-color','none');
  }
  

}
